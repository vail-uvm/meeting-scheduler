import argparse
import calendar
import itertools
import random

import pandas as pd
import requests
from bs4 import BeautifulSoup

str2day_of_week = dict(
    [
        *zip((x.lower() for x in calendar.day_name), range(7)),
        *zip((x.lower() for x in calendar.day_abbr), range(7)),
    ]
)


def get_parser():
    parser = argparse.ArgumentParser(
        description="Creates a semester meeting schedule for VaiL weekly meetings.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "start_date",
        help="The starting date of the schedule, usually the first day of a semester.",
    )
    parser.add_argument(
        "end_date",
        help="The last date of the schedule, usually the last day of classes for a semester.",
    )
    parser.add_argument(
        "meeting_day",
        help="The day of the week that the meeting should take place on.",
    )
    parser.add_argument(
        "meeting_time", help="The time slot that the meeting should take place at.",
    )

    parser.add_argument(
        "--meeting_types",
        help="Primary meeting items.",
        nargs="*",
        default=("Reading Group", "Research Presentation"),
    )

    parser.add_argument(
        "--calendar",
        help="The calendar type to base the schedule on.",
        choices=["uvm", "simple"],
        type=lambda x: x.lower().strip(),
        default="uvm",
    )

    return parser


def make_schedule(
    start_date,
    end_date,
    meeting_day,
    meeting_time,
    meeting_types=("Reading Group", "Research Presentation"),
    calendar="uvm",
):
    if calendar == "uvm":
        uvm_business_day = pd.offsets.CustomBusinessDay(holidays=get_uvm_holidays())
        dates = pd.date_range(
            f"{start_date} {meeting_time}",
            f"{end_date} {meeting_time}",
            freq=uvm_business_day,
        )
    elif calendar == "simple":
        dates = pd.date_range(
            f"{start_date} {meeting_time}",
            f"{end_date} {meeting_time}",
        )
    else:
        raise ValueError(f"Unexpected value encountered for calendar ({calendar}), should be one of {{'uvm', 'simple'}}.")

    meeting_day = str2day_of_week[meeting_day.lower()]
    meeting_dates = dates[dates.dayofweek == meeting_day]
    with open("members.csv", "r") as f:
        members = [x.strip() for x in f]

    possible_meetings = list(itertools.product(meeting_types, members))
    random.shuffle(possible_meetings)

    grouped_meetings = []
    for meeting_type in meeting_types:
        grouped_meetings.append([x for x in possible_meetings if x[0] == meeting_type])

    schedule = list(zip(meeting_dates, round_robin(*grouped_meetings)))

    return [(a, b, c) for (a, (b, c)) in schedule]


def get_uvm_holidays(year=None):
    if year is None:
        date = pd.Timestamp.now()
        if date.month < 3:
            year = date.year - 1
        else:
            year = date.year

    page = requests.get(
        f"https://www.uvm.edu/registrar/uvm-academic-calendar-{year}-{year + 1}"
    )
    soup = BeautifulSoup(page.text, "html.parser")

    tables = [x for x in soup.find_all("table", "responsive1")]
    events = sum((parse_uvm_date_table(table) for table in tables), [])

    return [
        pd.to_datetime(event[1])
        for event in events
        if ("Holiday" in event[0]) or ("Recess" in event[0])
    ]


def parse_uvm_date_table(table):
    year = table.caption.text.strip(" *").split()[-1]

    # The following snippet is adapted from:
    #   https://stackoverflow.com/questions/23377533/python-beautifulsoup-parsing-table
    data = []
    rows = table.find_all("tr")
    for row in rows:
        values = row.find_all("td")
        values = [x.text.strip() for x in values if x.text.strip()]
        if values:
            event = values[0]
            dates = parse_dates(values[1], year)
            data.extend([[event, date] for date in dates])
    return data


def parse_dates(date, year):
    date = date.strip("*")
    if "," in date:
        month, *days = date.replace(",", " ").split()
        dates = [f"{month} {day}, {year}" for day in days]
    elif "-" in date:
        month, start, end = date.replace("-", " ").split()
        dates = [f"{month} {day}, {year}" for day in range(int(start), int(end) + 1)]
    else:
        dates = [f"{date}, {year}"]
    return dates


def output_schedule(schedule, file="schedule.csv"):
    with open(file, "w") as f:
        for timestamp, flavor, presenter in schedule:
            print(f"{timestamp}, {flavor}, {presenter}", file=f)


def round_robin(*iterables):
    """
    Borrowed from:
        https://docs.python.org/3/library/itertools.html#itertools.cycle
    roundrobin('ABC', 'D', 'EF') --> A D E B F C
    Recipe credited to George Sakkis
    """
    num_active = len(iterables)
    nexts = itertools.cycle(iter(it).__next__ for it in iterables)
    while num_active:
        try:
            for next in nexts:
                yield next()
        except StopIteration:
            # Remove the iterator we just exhausted from the cycle.
            num_active -= 1
            nexts = itertools.cycle(itertools.islice(nexts, num_active))


def main():
    args = get_parser().parse_args()
    output_schedule(make_schedule(**vars(args)))


if __name__ == "__main__":
    main()
